import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions/authActions";
import { Redirect } from "react-router";
class Login extends Component {
  constructor() {
    super();
    this.state = {
      UserName: "",
      password: "",
    
    };
  }
  onSubmit = (e) => {
    e.preventDefault();
    const userData = {
      UserName: this.state.UserName,
      password: this.state.password,
    };
    this.props.loginUser(userData, this.props.history);
    //var decoded = jwt_decode(localStorage.getItem("jwtToken"));
    //console.log(this.props.auth);
  };



  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="join-form">
        <Form onSubmit={this.onSubmit}>
          <h3 className="text-center">
            <span className="font-weight-bold">POLDY</span>
          </h3>

          <FormGroup>
            <Label>Kullanıcı Adı</Label>
            <Input
              type="text"
              placeholder="Lütfen E-Mail adresinizi giriniz"
              name="UserName"
              value={this.state.UserName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>Şifre</Label>
            <Input
              type="password"
              placeholder="Lütfen şifrenizi giriniz"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
              required
            />
          </FormGroup>

          <Button className="btn-block bn-lg" color="primary">
            Giriş Yap
          </Button>
          
        </Form>
        </div>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps, { loginUser })(Login);
