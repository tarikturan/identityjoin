import React, { Component } from "react";
import alertify from "alertifyjs";
import { url } from "../../url";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { registerUser } from "../../redux/actions/authActions";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      passwordAgain: "",
      companyId: "",
      firstName: "",
      lastName: "",
      userName: "",
      redirect: false,
      formVisible: false,
    };
  }
  onSubmit = (e) => {
    e.preventDefault();
    let company;
    let rank;

    if(this.state.formVisible){
      company = parseInt(this.state.companyId)
      rank = true
    }else{
      company = this.props.auth.user.user.companyId;
      rank = false;
    }
    const userData = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      companyId: company,
      password: this.state.password,
      passwordAgain: this.state.passwordAgain,
      userName: this.state.userName,
      email: this.state.email,
    }
    this.props.registerUser(userData,rank, this.props.history);
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onFormVisible = () => {
    let visible = false;
    if(this.props.auth.isAuthenticated){
      if (this.props.auth.user.claim[0].value === "PoldyAdmin") {
        visible = true;
      }
    }
    this.setState({formVisible: visible});
  }

  registerUser(userData) {
    return function (dispatch) {
      fetch(
        url + "register?password=" + userData.password,
        userData
      ).then((r) => alertify.success("Departman Başarıyla Silindi"));
    };
  }
  componentDidMount() {
    this.onFormVisible();
  }
  render() {
    
    return (
      <div className="add-form col-sm-4">
        <Form className="row justify-content-center" onSubmit={this.onSubmit}>
          <h3 className="text-center col-sm-12">
            <span className="font-weight-bold">POLDY</span>
          </h3>
          
          <FormGroup>
            <Label>Ad</Label>
            <Input
              type="text"
              placeholder="Adınız"
              name="firstName"
              value={this.state.firstName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>Soyad</Label>
            <Input
              type="text"
              placeholder="Soyadınız"
              name="lastName"
              value={this.state.lastName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>Kullanıcı Adı</Label>
            <Input
              type="text"
              placeholder="Adınız"
              name="userName"
              value={this.state.userName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>E-Mail</Label>
            <Input
              type="email"
              placeholder="Lütfen E-Mail adresinizi giriniz"
              name="email"
              value={this.state.email}
              onChange={this.onChange}
              required
            />
            
          </FormGroup>
          
          <FormGroup>
            <Label>Şifre</Label>
            <Input
              type="password"
              placeholder="Lütfen şifreyi giriniz"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup>
            <Label>Şifre Onay</Label>
            <Input
              type="password"
              placeholder="Lütfen şifreyi tekrar giriniz"
              name="passwordAgain"
              value={this.state.passwordAgain}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          {
            this.state.formVisible ?
              <FormGroup
                className="companyCode">
                <Label>Şirket Kodu</Label>
                <Input
                  type="number"
                  placeholder="Şirket Kodu"
                  name="companyId"
                  value={this.state.companyId}
                  onChange={this.onChange}
                  required
                />
              </FormGroup> : null
          }
          <Button className="btn-block bn-lg" color="success">
            Ekle
          </Button>
        </Form>
      </div>
    )}
  }


const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps, {registerUser})(Register);
