import React, { Component } from "react";
import alertify from "alertifyjs";
import { url } from "../../url";
import axios from "axios";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";

class AddUser extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      passwordAgain: "",
      companyId: "",
      firstName: "",
      lastName: "",
      userName: "",
      role: "",
      
    };
  }

  onSubmit = (e) => {
    debugger;
    e.preventDefault();
    const userData = {
      companyName: this.state.companyName,
      companyId: parseInt(this.state.companyId),
    };
    if (userData.password != userData.passwordAgain) {
      alert("Şifreler eşleşmiyor");
      return;
    }
    const url1 = url + "register?password=" + userData.password;
    axios.post(url1, userData).then((res) => {
      if (res.data) {
        this.setState({ redirect: true });
        alert("Kullanıcı başarıyla eklendi.");
      } else if (res.data === false) {
        this.setState({ redirect: false });
        alert("Kayıt Başarısız");
      }
    });
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  registerUser(userData) {
    debugger;

    return function (dispatch) {
      fetch(
        url + "register?password=" + userData.password,
        userData
      ).then((r) => alertify.success("Departman Başarıyla Silindi"));
    };
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
    return (
      <div className="add-form col-sm-4">
        <Form className="row justify-content-center" onSubmit={this.onSubmit}>
          <h3 className="text-center col-sm-12">
            <span className="font-weight-bold">Kullanıcı Ekle</span>
          </h3>
          <FormGroup className="col-sm-12">
            <Label>Adı</Label>
            <Input
              type="text"
              placeholder="Kullanıcının adını giriniz..."
              name="firstName"
              value={this.state.firstName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup className="col-sm-12">
            <Label>Soyadı</Label>
            <Input
              type="text"
              placeholder="Kullanıcının soyadını giriniz..."
              name="lastName"
              value={this.state.lastName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup className="col-sm-12">
            <Label>Kullanıcı Adı</Label>
            <Input
              type="text"
              placeholder="Kullanıcı için benzersiz bir kullanıcı adı belirleyiniz..."
              name="userName"
              value={this.state.userName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup className="col-sm-12">
            <Label>E-Mail</Label>
            <Input
              type="email"
              placeholder="E-Mail"
              name="email"
              value={this.state.email}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          
          <FormGroup className="col-sm-12">
            <label for="userRole">Rol</label>
            <select class="form-control" id="userRole">
              <option>Admin</option>
              <option>Kullanıcı</option>
            </select>
          </FormGroup>
          <FormGroup className="col-sm-12">
            <Label>Şifre</Label>
            <Input
              type="password"
              placeholder="Şirket Kodu"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
              required
            />
          </FormGroup>

          <FormGroup className="col-sm-12">
            <Label>Şifreyi Onaylayın</Label>
            <Input
              type="password"
              placeholder="Şirket Kodu"
              name="passwordAgain"
              value={this.state.passwordAgain}
              onChange={this.onChange}
              required
            />
          </FormGroup>

          <FormGroup className="col-sm-3 ">
            <Button className="btn-block btn-sm " color="success">
              Ekle
            </Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  auth: state.auth,
});
export default connect(mapStateToProps)(AddUser);
