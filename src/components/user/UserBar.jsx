import React, { Component } from "react";

import "../../Sidebar.css";

class UserBar extends Component {
  state = {
    drawerPos: 1,
  };

  handleDrawer = () => {
    if (this.state.drawerPos < 2) {
      this.setState((state) => ({
        drawerPos: state.drawerPos + 1,
      }));
    } else {
      this.setState({
        drawerPos: 0,
      });
    }
  };
  render() {
    let drawerClass = [];
    let mainClass = [];
    if (this.state.drawerPos === 1) {
      drawerClass.push("drawerMin");
      mainClass.push("mainMin");
    } else if (this.state.drawerPos === 2) {
      drawerClass.push("drawerOpen");
      mainClass.push("mainOpen");
    } else {
      drawerClass = [];
      mainClass = [];
    }

    return (
      <div className="AdminSidebar">
        <navbar>
          {" "}
          <i className="material-icons" onClick={this.handleDrawer}>
            menu
          </i>{" "}
          <img
            className="logo"
            width="100px"
            src="https://www.poldy.com.tr/Content/gfx/logo-white.png"
            alt="POLDY LOGO"
          />{" "}
        </navbar>
        <aside className={drawerClass.join(" ")}>
          <ul>
            <li>
              <i className="material-icons ">dashboard</i>
              <span>Dashboard</span>
            </li>
           
          </ul>
        </aside>
        <main className={mainClass.join(" ")}>Kullanıcı Paneli</main>
        <link
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet"
        ></link>
      </div>
    );
  }
}

export default UserBar;
