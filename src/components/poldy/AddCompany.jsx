import React, { Component } from "react";
import alertify from "alertifyjs";
import { url } from "../../url";
import axios from "axios";
import { Redirect } from "react-router";

import { Button, Form, FormGroup, Label, Input } from "reactstrap";

class AddCompany extends Component {
  constructor() {
    super();
    this.state = {
      companyName: "",
      companyId: "",

      redirect: false,
    };
  }

  onSubmit = (e) => {
    debugger;
    e.preventDefault();
    const userData = {
      companyName: this.state.companyName,
      companyId: parseInt(this.state.companyId),
    };
    if (userData.password != userData.passwordAgain) {
      alert("Şifreler eşleşmiyor");
      return;
    }
    const url1 = url + "register?password=" + userData.password;
    axios.post(url1, userData).then((res) => {
      if (res.data) {
        this.setState({ redirect: true });
        alert("Kullanıcı başarıyla eklendi.");
      } else if (res.data === false) {
        this.setState({ redirect: false });
        alert("Kayıt Başarısız");
      }
    });
  };

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  registerUser(userData) {
    debugger;

    return function (dispatch) {
      fetch(
        url + "register?password=" + userData.password,
        userData
      ).then((r) => alertify.success("Departman Başarıyla Silindi"));
    };
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
    return (
      <div className="d-flex add-form container">
        <Form className=" justify-content-start" onSubmit={this.onSubmit}>
          <h3 className="text-center col-sm-12">
            <span className="font-weight-bold">Şirket Ekle</span>
          </h3>
          <FormGroup >
            <Label>Şirket Adı</Label>
            <Input
              type="text"
              placeholder="Şirket Adı"
              name="companyName"
              value={this.state.companyName}
              onChange={this.onChange}
              required
            />
          </FormGroup>
          <FormGroup >
            <Label>Şirket Kodu</Label>
            <Input
              type="number"
              placeholder="Şirket Kodu"
              name="companyId"
              value={this.state.companyId}
              onChange={this.onChange}
              required
            />
          </FormGroup>

          <FormGroup >
            <Button className="btn-block btn-sm " color="success">
              Ekle
            </Button>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default AddCompany;
