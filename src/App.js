import React from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import Login from "../src/components/auth/Login";
import Register from "./components/auth/Register";
import AdminBar from "./components/admin/AdminBar";
import PoldyBar from "./components/poldy/PoldyBar";
import { connect } from "react-redux";
import UserBar from "./components/user/UserBar";

class App extends React.Component {
  render(){
    debugger;
  return (
    <div>
      <Switch>
        <Route path="/Register" component={Register}/>
        <Route path="/poldy" component={PoldyBar}/>
        <Route path="/user" component={UserBar}/>
        <Route path="/company" component={AdminBar}/>
        <Route path="/Login" component={Login} />
        <Route path="/" component={Login} />
      </Switch>
    </div>
  );
}}
const mapStateToProps = (state) => ({
  auth: state.auth,
});

export default connect(mapStateToProps)(App);
