import { ADD_COMPANY } from '../actions/actionTypes';



export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_COMPANY:
            return {
                company: action.payload
            };
        default:
            return state;
    }
}