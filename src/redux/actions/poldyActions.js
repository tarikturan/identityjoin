import axios from "axios";
import { url } from "../../url";
import { ADD_COMPANY } from "./actionTypes";




export function addCompany(company) {

    return function (dispatch) {
        var url1 = url + "Company/add"
        axios
            .post(url1, company)
            .catch(err => console.log(err))
    }
}
