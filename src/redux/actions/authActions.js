import axios from "axios";
import { url } from "../../url";
import { SET_CURRENT_USER } from "./actionTypes";
import setAuthToken from "../../utils/setAuthToken";
import alertify from "alertifyjs";
//import { Redirect } from "react-router";

export const registerUser = (userData,rank, history) => (dispatch) => {
  if (userData.password !== userData.passwordAgain) {
    return alert("Şifreler eşleşmiyor");
  }
  var position = "";
  if (rank) {
    position = "CompanyAdmin";
  } else {
    position = "user";
  }
  const url1 = url + "Account/register?position=" + position;
  axios
    .post(url1, userData)
    .then(alert("Kullanıcı başarıyla eklendi"))
    .catch((e) => console.log(e));
};

export const loginUser = (userData, history) => (dispatch) => {
  axios
    .post(url + "Account/login", userData)
    .then((res) => {
      //console.log(res.data.claim[0].value);
      const token = res.data.token;
      localStorage.setItem("jwtToken", token);
      setAuthToken(token);
      dispatch(setCurrentUser(res.data));
      directs(res.data.claim[0].value,history);
    })
    .catch((e) => alert(e));
};
export const directs=(claim,history)=>{
  if(claim==="CompanyAdmin"){
      history.push("/company");
  }if(claim==="PoldyAdmin"){
    history.push("/poldy");
  }if(claim==="user"){
    history.push("/user");
  }
}

export const setCurrentUser = (decoded) => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded,
  };
};
